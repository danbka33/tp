﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DistanceLearning.Models;
using Microsoft.AspNetCore.Authorization;

namespace DistanceLearning.Controllers
{
    [Authorize(Roles = "admin,teacher")]
    public class TestAnswerVariantsController : Controller
    {
        private readonly ApplicationContext _context;

        public TestAnswerVariantsController(ApplicationContext context)
        {
            _context = context;
        }

        // GET: TestAnswerVariants
        public async Task<IActionResult> Index(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            TestQuestion question = _context.TestQuestions.Where(m => m.TestQuestionId == id).First();

            Test test = _context.Tests.Where(m => m.Id == question.TestId).First();

            ViewData["TestQuestionId"] = id;
            ViewData["TestName"] = test.Name;
            ViewData["QuestionName"] = question.Description;

            var applicationContext = _context.TestAnswerVariants.Include(t => t.TestQuestion).Where(t => t.TestQuestionId == id);
            return View(await applicationContext.ToListAsync());
        }

        // GET: TestAnswerVariants/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var testAnswerVariant = await _context.TestAnswerVariants
                .Include(t => t.TestQuestion)
                .FirstOrDefaultAsync(m => m.TestAnswerVariantId == id);
            if (testAnswerVariant == null)
            {
                return NotFound();
            }

            return View(testAnswerVariant);
        }

        // GET: TestAnswerVariants/Create
        public IActionResult Create(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ViewData["TestQuestionId"] = id;

            return View();
        }

        // POST: TestAnswerVariants/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Description,TestQuestionId")] TestAnswerVariant testAnswerVariant)
        {
            if (ModelState.IsValid)
            {
                _context.Add(testAnswerVariant);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index), new { id = testAnswerVariant.TestQuestionId });
            }
            ViewData["TestQuestionId"] = new SelectList(_context.TestQuestions, "Id", "Id", testAnswerVariant.TestQuestionId);
            return View(testAnswerVariant);
        }

        // GET: TestAnswerVariants/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var testAnswerVariant = await _context.TestAnswerVariants.FindAsync(id);
            if (testAnswerVariant == null)
            {
                return NotFound();
            }
            ViewData["TestQuestionId"] = new SelectList(_context.TestQuestions, "Id", "Id", testAnswerVariant.TestQuestionId);
            return View(testAnswerVariant);
        }

        // POST: TestAnswerVariants/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Description,TestQuestionId")] TestAnswerVariant testAnswerVariant)
        {
            if (id != testAnswerVariant.TestAnswerVariantId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(testAnswerVariant);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TestAnswerVariantExists(testAnswerVariant.TestAnswerVariantId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["TestQuestionId"] = new SelectList(_context.TestQuestions, "Id", "Id", testAnswerVariant.TestQuestionId);
            return View(testAnswerVariant);
        }

        public async Task<IActionResult> MarkTrue(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            TestAnswerVariant testAnswerVariant = _context.TestAnswerVariants.Find(id);

            TestQuestion testQuestion = _context.TestQuestions.Find(testAnswerVariant.TestQuestionId);

            testQuestion.TrueAnswerId = id;

            _context.Update(testQuestion);

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index), new { id = testQuestion.TestQuestionId });
        }

        // GET: TestAnswerVariants/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var testAnswerVariant = await _context.TestAnswerVariants
                .Include(t => t.TestQuestion)
                .FirstOrDefaultAsync(m => m.TestAnswerVariantId == id);
            if (testAnswerVariant == null)
            {
                return NotFound();
            }

            return View(testAnswerVariant);
        }

        // POST: TestAnswerVariants/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var testAnswerVariant = await _context.TestAnswerVariants.FindAsync(id);
            _context.TestAnswerVariants.Remove(testAnswerVariant);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TestAnswerVariantExists(int id)
        {
            return _context.TestAnswerVariants.Any(e => e.TestAnswerVariantId == id);
        }
    }
}
