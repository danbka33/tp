﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DistanceLearning.Models;
using Microsoft.AspNetCore.Authorization;

namespace DistanceLearning.Controllers
{
    public class TestsController : Controller
    {
        private readonly ApplicationContext _context;

        public TestsController(ApplicationContext context)
        {
            _context = context;
        }

        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Managment()
        {
            var query = from test in _context.Tests
                        join subject in _context.Subjects on test.SubjectId equals subject.Id
                        select test;
            return View("index", await query.ToListAsync());
        }

        [Authorize(Roles = "student")]
        public async Task<IActionResult> TestResults(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            User user = _context.Users.Where(m => m.Email == User.Identity.Name).First();

            int count = _context.TestQuestions.FromSqlRaw($"SELECT TestQuestions.* FROM TestQuestions " +
                    $"LEFT JOIN TestResults ON TestResults.TestQuestionId = TestQuestions.TestQuestionId AND TestResults.UserId =  " + user.Id +
                    $"WHERE TestResults.Id IS NULL AND TestQuestions.TestId = " + id).Count();

            if(count == 0)
            {
                List<TestResult> results = await _context.TestResults.Include(t => t.TestQuestion).ToListAsync();

                int scores = 0;
                int totalScore = 0;

                foreach(TestResult result in results)
                {
                    if(result.Correct == true)
                    {
                        scores += result.TestQuestion.TrueAnswerScore;
                    }

                    totalScore += result.TestQuestion.TrueAnswerScore;
                }

                ViewData["Scores"] = scores;
                ViewData["TotalScore"] = totalScore;

                return View("TestResults");
            }

            return RedirectToAction(nameof(TestResults), new { id = id });
        }

        [Authorize(Roles = "student")]

        public IActionResult TakeTest(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            User user = _context.Users.Where(m => m.Email == User.Identity.Name).First();
            try
            {
                TestQuestion testQuestion = _context.TestQuestions.FromSqlRaw($"SELECT TestQuestions.* FROM TestQuestions " +
                    $"LEFT JOIN TestResults ON TestResults.TestQuestionId = TestQuestions.TestQuestionId AND TestResults.UserId =  " + user.Id +
                    $"WHERE TestResults.Id IS NULL AND TestQuestions.TestId = " + id).First();

                Test test = _context.Tests.Find(testQuestion.TestId);

                TestAnswerForm testAnswerForm = new TestAnswerForm();
                testAnswerForm.Question = testQuestion.TestQuestionId;
                testAnswerForm.Answers = _context.TestAnswerVariants.Where(c => c.TestQuestionId == testQuestion.TestQuestionId).ToList();

                ViewData["QuestionDescription"] = testQuestion.Description;
                ViewData["SubjectId"] = test.SubjectId;

                return View("takeTest", testAnswerForm);
            } catch(InvalidOperationException e)
            {
                return RedirectToAction(nameof(TestResults), new { id = id });
            }
        }

        [Authorize(Roles = "student")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> TakeTest(int id, [Bind("Response,Question")] TestAnswerForm testAnswerForm)
        {
            if (ModelState.IsValid)
            {
                User user = _context.Users.Where(m => m.Email == User.Identity.Name).First();

                TestQuestion testQuestion = _context.TestQuestions.Find(testAnswerForm.Question);

                TestResult testResult = new TestResult();

                testResult.TestQuestionId = testQuestion.TestQuestionId;
                testResult.UserId = user.Id;
                testResult.Correct = testQuestion.TrueAnswerId == testAnswerForm.Response;
                    
                _context.Add(testResult);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(TakeTest), new { id = id });
            }

            return RedirectToAction(nameof(TakeTest), new { id = id });
        }

        [Authorize(Roles = "admin, teacher")]
        public async Task<IActionResult> ShowStudentResults(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            //int totalScore = 0;

            var totalScore = (from testQuestion in _context.TestQuestions
                             where testQuestion.TestId == id
                             select testQuestion.TrueAnswerScore).Sum();

           // foreach(var traw in totalScoreRaw)
            //{
            //    totalScore += traw;
           // }

            List < User > users = await _context.Users.FromSqlRaw($"SELECT U.* FROM Tests " +
                $"JOIN UsersSubject US on Tests.SubjectId = US.SubjectId " +
                $"JOIN Users U on US.UserId = U.Id " +
                $"WHERE Tests.Id = " + id + " AND U.UserTypeId = 2").ToListAsync();

            List<TestStudentResultTable> results = new List<TestStudentResultTable>();

            foreach(User user in users)
            {
                var test = _context.TestStudentResultTables.FromSqlRaw($"SELECT SUM(TestQuestions.TrueAnswerScore) as Score, UserId FROM Tests " +
                    $"JOIN TestQuestions ON Tests.Id = TestQuestions.TestId " +
                    $"JOIN TestResults ON TestResults.TestQuestionId = TestQuestions.TestQuestionId " +
                    $"WHERE Tests.Id = "+id+" AND TestResults.UserId =  " + user.Id +
                    $" GROUP BY UserId").FirstOrDefault();

                if(test != null)
                {
                    results.Add(new TestStudentResultTable { User = user, Score = test.Score });
                } else
                {
                    results.Add(new TestStudentResultTable { User = user, Score = 0 });
                }
            }

            ViewData["TotalScore"] = totalScore;

            return View("ShowStudentResults", results);
        }

        // GET: Tests
        [Authorize(Roles = "admin,teacher,student")]
        public async Task<IActionResult> Index(int id)
        {
            Subject subject = _context.Subjects.Where(m => m.Id == id).First();

            ViewData["Subject"] = subject.Name;

            User user = _context.Users.Where(m => m.Email == User.Identity.Name).First();

            var query = from test in _context.Tests
                        //join subject in _context.Subjects on test.SubjectId equals subject.Id
                        join userSubject in _context.UsersSubject on test.SubjectId equals userSubject.SubjectId
                        where userSubject.UserId == user.Id && userSubject.SubjectId == id
                        select test;

            var list = await query.ToListAsync();

            foreach(var item in list)
            {
                int count = _context.TestQuestions.FromSqlRaw($"SELECT TestQuestions.* FROM TestQuestions " +
                    $"LEFT JOIN TestResults ON TestResults.TestQuestionId = TestQuestions.TestQuestionId AND TestResults.UserId =  " + user.Id +
                    $"WHERE TestResults.Id IS NULL AND TestQuestions.TestId = " + item.Id).Count();

                    item.completeForUser = count == 0;
            }

            return View(list);
        }

        // GET: Tests/Details/5
        [Authorize(Roles = "admin,teacher")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var test = await _context.Tests
                .Include(t => t.Subject)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (test == null)
            {
                return NotFound();
            }

            return View(test);
        }

        [Authorize(Roles = "admin,teacher")]
        // GET: Tests/Create
        public IActionResult Create()
        {
            if (User.IsInRole("teacher"))
            {
                User user = _context.Users.Where(m => m.Email == User.Identity.Name).First();

                ViewData["SubjectId"] = new SelectList(_context.Subjects.FromSqlRaw($"SELECT Subjects.* FROM Subjects " +
                    $"JOIN UsersSubject US on Subjects.Id = US.SubjectId " +
                    $"WHERE US.UserId = " + user.Id + $" GROUP BY US.SubjectId, Subjects.Id, Subjects.Name"), "Id", "Name");
            }
            else
            {
                ViewData["SubjectId"] = new SelectList(_context.Subjects, "Id", "Name");
            }
            return View();
        }

        [Authorize(Roles = "admin,teacher")]
        // POST: Tests/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,SubjectId")] Test test)
        {
            if (ModelState.IsValid)
            {
                _context.Add(test);
                await _context.SaveChangesAsync();

                if(User.IsInRole("admin"))
                {
                    return RedirectToAction(nameof(Managment));
                }

                return RedirectToAction("Index", "Subjects");
            }
            if (User.IsInRole("teacher"))
            {
                User user = _context.Users.Where(m => m.Email == User.Identity.Name).First();

                ViewData["SubjectId"] = new SelectList(_context.Subjects.FromSqlRaw($"SELECT Subjects.* FROM Subjects " +
                    $"JOIN UsersSubject US on Subjects.Id = US.SubjectId " +
                    $"WHERE US.UserId = " + user.Id + $" GROUP BY US.SubjectId, Subjects.Id, Subjects.Name"), "Id", "Name");
            }
            else
            {
                ViewData["SubjectId"] = new SelectList(_context.Subjects, "Id", "Name");
            }

            return View(test);
        }

        [Authorize(Roles = "admin,teacher")]
        // GET: Tests/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var test = await _context.Tests.FindAsync(id);
            if (test == null)
            {
                return NotFound();
            }
            if (User.IsInRole("teacher"))
            {
                User user = _context.Users.Where(m => m.Email == User.Identity.Name).First();

                ViewData["SubjectId"] = new SelectList(_context.Subjects.FromSqlRaw($"SELECT Subjects.* FROM Subjects " +
                    $"JOIN UsersSubject US on Subjects.Id = US.SubjectId " +
                    $"WHERE US.UserId = " + user.Id + $" GROUP BY US.SubjectId, Subjects.Id, Subjects.Name"), "Id", "Name");
            }
            else
            {
                ViewData["SubjectId"] = new SelectList(_context.Subjects, "Id", "Name");
            }
            return View(test);
        }

        [Authorize(Roles = "admin,teacher")]
        // POST: Tests/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,SubjectId")] Test test)
        {
            if (id != test.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(test);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TestExists(test.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            if (User.IsInRole("teacher"))
            {
                User user = _context.Users.Where(m => m.Email == User.Identity.Name).First();

                ViewData["SubjectId"] = new SelectList(_context.Subjects.FromSqlRaw($"SELECT Subjects.* FROM Subjects " +
                    $"JOIN UsersSubject US on Subjects.Id = US.SubjectId " +
                    $"WHERE US.UserId = " + user.Id + $" GROUP BY US.SubjectId, Subjects.Id, Subjects.Name"), "Id", "Name");
            }
            else
            {
                ViewData["SubjectId"] = new SelectList(_context.Subjects, "Id", "Name");
            }
            return View(test);
        }

        [Authorize(Roles = "admin,teacher")]
        // GET: Tests/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var test = await _context.Tests
                .Include(t => t.Subject)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (test == null)
            {
                return NotFound();
            }

            return View(test);
        }

        [Authorize(Roles = "admin,teacher")]
        // POST: Tests/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var test = await _context.Tests.FindAsync(id);
            _context.Tests.Remove(test);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TestExists(int id)
        {
            return _context.Tests.Any(e => e.Id == id);
        }
    }
}
