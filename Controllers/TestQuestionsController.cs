﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DistanceLearning.Models;
using Microsoft.AspNetCore.Authorization;

namespace DistanceLearning.Controllers
{
    [Authorize(Roles = "admin,teacher")]
    public class TestQuestionsController : Controller
    {
        private readonly ApplicationContext _context;

        public TestQuestionsController(ApplicationContext context)
        {
            _context = context;
        }

        // GET: TestQuestions
        public async Task<IActionResult> Index(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Test test = _context.Tests.Where(m => m.Id == id).First();

            ViewData["TestId"] = id;
            ViewData["TestName"] = test.Name;

            var applicationContext = _context.TestQuestions.Include(t => t.Test).Where(t => t.TestId == id);
            return View(await applicationContext.ToListAsync());
        }

        // GET: TestQuestions/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var testQuestion = await _context.TestQuestions
                .Include(t => t.Test)
                .FirstOrDefaultAsync(m => m.TestQuestionId == id);
            if (testQuestion == null)
            {
                return NotFound();
            }

            return View(testQuestion);
        }

        // GET: TestQuestions/Create
        public IActionResult Create(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ViewData["TestId"] = id;

            return View();
        }

        // POST: TestQuestions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Description,TestId,TrueAnswerScore")] TestQuestion testQuestion)
        {
            if (ModelState.IsValid)
            {
                _context.Add(testQuestion);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index), new { id = testQuestion.TestId } );
            }
            ViewData["TestId"] = new SelectList(_context.Tests, "Id", "Name", testQuestion.TestId);
            return View(testQuestion);
        }

        // GET: TestQuestions/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var testQuestion = await _context.TestQuestions.FindAsync(id);
            if (testQuestion == null)
            {
                return NotFound();
            }
            ViewData["TestId"] = new SelectList(_context.Tests, "Id", "Id", testQuestion.TestId);
            return View(testQuestion);
        }

        // POST: TestQuestions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Description,TestId,TrueAnswerId,TrueAnswerScore")] TestQuestion testQuestion)
        {
            if (id != testQuestion.TestQuestionId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(testQuestion);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TestQuestionExists(testQuestion.TestQuestionId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["TestId"] = new SelectList(_context.Tests, "Id", "Id", testQuestion.TestId);
            return View(testQuestion);
        }

        // GET: TestQuestions/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var testQuestion = await _context.TestQuestions
                .Include(t => t.Test)
                .FirstOrDefaultAsync(m => m.TestQuestionId == id);
            if (testQuestion == null)
            {
                return NotFound();
            }

            return View(testQuestion);
        }

        // POST: TestQuestions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var testQuestion = await _context.TestQuestions.FindAsync(id);
            _context.TestQuestions.Remove(testQuestion);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TestQuestionExists(int id)
        {
            return _context.TestQuestions.Any(e => e.TestQuestionId == id);
        }
    }
}
