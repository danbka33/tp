﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DistanceLearning.Models;
using Microsoft.AspNetCore.Authorization;

namespace DistanceLearning.Controllers
{
    [Authorize(Roles = "admin")]
    public class UserSubjectsController : Controller
    {
        private readonly ApplicationContext _context;

        public UserSubjectsController(ApplicationContext context)
        {
            _context = context;
        }

        // GET: UserSubjects
        public async Task<IActionResult> Index()
        {
            var applicationContext = _context.UsersSubject.Include(u => u.Subject).Include(u => u.User);
            return View(await applicationContext.ToListAsync());
        }

        // GET: UserSubjects/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var userSubject = await _context.UsersSubject
                .Include(u => u.Subject)
                .Include(u => u.User)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (userSubject == null)
            {
                return NotFound();
            }

            return View(userSubject);
        }

        // GET: UserSubjects/Create
        public IActionResult Create()
        {
            ViewData["SubjectId"] = new SelectList(_context.Subjects, "Id", "Name");
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Name");
            return View();
        }

        // POST: UserSubjects/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,UserId,SubjectId")] UserSubject userSubject)
        {
            if (ModelState.IsValid)
            {
                _context.Add(userSubject);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["SubjectId"] = new SelectList(_context.Subjects, "Id", "Name", userSubject.SubjectId);
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Name", userSubject.UserId);
            return View(userSubject);
        }

        // GET: UserSubjects/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var userSubject = await _context.UsersSubject.FindAsync(id);
            if (userSubject == null)
            {
                return NotFound();
            }
            ViewData["SubjectId"] = new SelectList(_context.Subjects, "Id", "Name", userSubject.SubjectId);
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Name", userSubject.UserId);
            return View(userSubject);
        }

        // POST: UserSubjects/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,UserId,SubjectId")] UserSubject userSubject)
        {
            if (id != userSubject.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(userSubject);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UserSubjectExists(userSubject.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["SubjectId"] = new SelectList(_context.Subjects, "Id", "Name", userSubject.SubjectId);
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Name", userSubject.UserId);
            return View(userSubject);
        }

        // GET: UserSubjects/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var userSubject = await _context.UsersSubject
                .Include(u => u.Subject)
                .Include(u => u.User)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (userSubject == null)
            {
                return NotFound();
            }

            return View(userSubject);
        }

        // POST: UserSubjects/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var userSubject = await _context.UsersSubject.FindAsync(id);
            _context.UsersSubject.Remove(userSubject);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool UserSubjectExists(int id)
        {
            return _context.UsersSubject.Any(e => e.Id == id);
        }
    }
}
