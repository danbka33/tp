﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DistanceLearning.Models
{
    public class TestQuestion
    {
        [Key]
        public int TestQuestionId { get; set; }

        public string Description { get; set; }

        [ForeignKey("TestId")]
        public Test Test { get; set; }

        public int TestId
        {
            get; set;
        }

        //public TestAnswerVariant? TrueAnswer { get; set; }

        public int? TrueAnswerId
        {
            get; set;
        }

        [DisplayName("Score")]
        public int TrueAnswerScore
        {
            get; set;
        }


        public List<TestAnswerVariant> answers { get; set; }
    }
}
