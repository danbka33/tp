﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DistanceLearning.Models
{
    public class TestAnswerVariant
    {
        [Key]
        public int TestAnswerVariantId { get; set; }
     
        public string Description { get; set; }

        public TestQuestion TestQuestion { get; set; }

        public int? TestQuestionId
        {
            get; set;
        }
    }
}
