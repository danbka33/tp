﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DistanceLearning.Models
{
    public class TestStudentResultTable
    {
        public User User { get; set; }

        //public int TotalScore { get; set; }

        public int Score { get; set; }
    }
}
