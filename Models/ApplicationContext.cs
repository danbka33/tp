﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DistanceLearning.Models
{
    public class ApplicationContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<UserType> UserTypes { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<UserSubject> UsersSubject { get; set; }
        public DbSet<Test> Tests { get; set; }
        public DbSet<TestQuestion> TestQuestions { get; set; }
        public DbSet<TestAnswerVariant> TestAnswerVariants { get; set; }
        public DbSet<TestResult> TestResults { get; set; }
        public DbSet<TestStudentResultTable> TestStudentResultTables { get; set; }

        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
        public static readonly Microsoft.Extensions.Logging.LoggerFactory _myLoggerFactory =
    new LoggerFactory(new[] {
        new Microsoft.Extensions.Logging.Debug.DebugLoggerProvider()
    });

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLoggerFactory(_myLoggerFactory);
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TestStudentResultTable>().HasNoKey();

            string adminRoleName = "admin";
            string studentRoleName = "student";
            string teacherRoleName = "teacher";

            string adminEmail = "admin@mail.ru";
            string adminPassword = "123456";

            Group pri117Group = new Group { Id = 1, Name = "Pri117" };

            // добавляем роли
            UserType adminRole = new UserType { Id = 1, Name = adminRoleName };
            UserType studentRole = new UserType { Id = 2, Name = studentRoleName };
            UserType teacherRole = new UserType { Id = 3, Name = teacherRoleName };

            User adminUser = new User { Id = 1, Email = adminEmail, Password = adminPassword, UserTypeId = adminRole.Id };
            User studentUser = new User { Id = 2, Email = adminEmail, Password = adminPassword, UserTypeId = studentRole.Id, GroupId = pri117Group.Id };

            modelBuilder.Entity<Group>().HasData(new Group[] { pri117Group });
            modelBuilder.Entity<UserType>().HasData(new UserType[] { adminRole, studentRole, teacherRole });
            modelBuilder.Entity<User>().HasData(new User[] { adminUser });
            base.OnModelCreating(modelBuilder);
        }
    }
}
