﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DistanceLearning.Models
{
    public class TestResult
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("TestQuestionId")]
        public TestQuestion TestQuestion { get; set; }

        public int TestQuestionId
        {
            get; set;
        }

        [ForeignKey("UserId")]
        public User User { get; set; }

        public int UserId { get; set; }

        public bool Correct { get; set; }
    }
}
