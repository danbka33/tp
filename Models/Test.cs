﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DistanceLearning.Models
{
    public class Test
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        [ForeignKey("SubjectId")]
        public Subject Subject { get; set; }

        public int SubjectId { get; set; }

        [NotMapped]
        public bool completeForUser { get; set; }
    }
}
