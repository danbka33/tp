﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DistanceLearning.Models
{
    public class TestAnswerForm
    {
        public int Question { get; set; }

        public int Response { get; set; }

        public List<TestAnswerVariant> Answers { get; set; }
    }
}
